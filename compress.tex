%\begin{figure}
%\centering
%\includegraphics[scale=.11]{comp-ratio}
%\caption{Compression ratios of nine file types. Higher compression ratio reduces more data volume.}
%\label{fig:comp-ratio}
%\vspace{-15pt}
%\end{figure}
 


PageSplit, originally devised to resolve the space waste problem (by small writes), can also be used in the context of other state-of-the-art SSD designs. As an example, with the help of PageSplit's fine-grained (sector) program unit, the design of \emph{data compression-based SSDs} becomes simple and feasible.



\subsection{Data Compression in SSD}
One way to prolong the lifespan of an SSD (which has a finite number of programs) is to reduce the data volume to be written in the underlying flash medium. Both academia \cite{compress-1,compress-2,compress-3,compress-5} and industry \cite{compress-4} have attempted to leverage data compression to shrink data sizes and in turn reduce the number of programs in SSDs.









\noindent \emph{\bf General strategy:~}One major strategy employed in data compression in SSDs is to generate a set of compressed data that can fit into a physical page. Since the basic flash write unit is the page, programming a single compressed data in a page wastes the remaining page space (note that the size of a compressed data is generally smaller than the page size). Therefore, combining multiple compressed data is necessary to make each page fully utilized and reduce the number of programs. However, this approach may not be effective in practice, since it assumes an ideal scenario and brings performance overheads.

%Since the size of a compressed data significantly varies depending on the compression ratio,



\begin{figure}
\centering
\vspace{-10pt}
\subfloat[Compression ratios]{\label{fig:comp-ratio}\rotatebox{0}{\includegraphics[scale=0.11]{comp-ratio2}}}
\hspace{1pt}
\subfloat[PageSplit+ example]{\label{fig:compress}\rotatebox{0}{\includegraphics[scale=0.12]{compress}}}
\vspace{-5pt}
\caption{(a) Compression ratios of nine file types (a higher ratio means more reduction in data size), and (b) PageSplit+, combining data compression and PageSplit.}
\vspace{-12pt}
\label{fig:arch-comp}
\end{figure}




%\begin{figure}
%\centering
%\includegraphics[scale=.11]{compress}
%\caption{PageSplit+ storage framework combining data compression and the PageSplit architecture.}
%\label{fig:compress}
%\vspace{-15pt}
%\end{figure}


\noindent \emph{\bf Unrealistic assumption on internal buffer:~}To generate a combination of the compressed data whose size is equal to the page size, a sufficient amount of (compressed) data has to be staged into a storage-internal buffer for a long period of time, before they are actually stored in the underlying flash. This is because (1) the size of compressed data can significantly vary depending on its compression ratio, and (2) one cannot anticipate the compression ratios of the following I/O writes. According to our analysis of a variety of workloads (see Figure \ref{fig:comp-ratio}), the compression ratios can vary significantly. In practice, most previous studies assume that there is a large window (i.e., a large space in the buffer) to keep a series of on-going data for a while without programming them to flash.


However, employing a very large window is not realistic in commodity SSDs, since the buffer in flash-based storages is limited-sized, and the pressure on such a small buffer can be very high due to the following reasons: (1) Its original purpose is to keep the flash management data such as mapping table, and the volume of this data increases as the storage capacity increases. (2) There is a trend towards employing more buffer space as a read cache to improve read I/O performance. (3) As the degree of storage-internal parallelism becomes high and the page size increases, we need a sufficient space to temporarily accommodate more and more read/write data. Hence, in a realistic scenario where the window size is very limited, it is difficult to combine compressed data to fit into a page, and the effectiveness of data compression may not be high as it leaves many page segments unused.


%(e.g., some flash storage formats such as MCS have very small buffers)

\noindent \emph{\bf Poor write performance:~}Even if the target storage employ a large buffer, the current approach can bring up a performance problem. By temporarily keeping a large amount of write data in the window and delaying them to be programmed to flash, the write performance can become worse. As the window size increases for better packing results, the storage performance becomes worse.







%\noindent \emph{\bf Complex Design:~}
%Packing multiple compressed data in a page increases the flash software complexity. For example, the bit-level information to locate a specific data segment within a physical page is not easy to maintain. 
%These two challenges prevent the design of the compression-based SSDs from being feasible and light-weight.


\begin{table}
\scriptsize
\begin{tabular}{|p{26pt} | p{27pt}  | p{35pt} | p{35pt}  | p{45pt} | }
\hline
\textbf{Workload} & \textbf{Write ratio(\%)} & \textbf{Avg. write size (KB)} & \textbf{Small write ratio(\%)} & \textbf{Page unalign. write ratio (\%)} \\
\hline \hline
cfs & 24.9 & 12.5 & 10.1 & 10.1 \\
\hline
fin & 76.8 & 3.7 & 92.5 & 97.4  \\
\hline
hm & 64.5 & 8.3 & 79.5 & 83.3  \\
\hline
mds & 7.1 & 13.8 & 64.5 & 75.8 \\
\hline
msnfs & 23.4 & 10.4  & 81 & 85.5 \\
\hline
prxy & 96.9 & 4.6 & 92.2 & 92.8  \\
\hline
prn & 98.1 & 6.1 & 86.6 & 87.7  \\
\hline
proj & 90.1 & 31.1 & 40 & 56.3  \\
\hline
usr & 91.9 & 8.7 & 64.3 & 69.8  \\
\hline
hpio & 100 & 6.5 & 98.9 & 99.2  \\
\hline
\end{tabular}
\vspace{-5pt}
\caption{Important characteristics of our workloads.}
\label{tab:workload-cha}
\vspace{-15pt}
\end{table}





\subsection{Benefits of Leveraging PageSplit}
PageSplit can be an alternative architecture to realize feasible and effective data compression-based SSDs, since its sector-based program unit can eliminate the high-overhead data packing process.

\noindent \emph{\bf Immediate programming:~}Since our fine-grained programming unit allows a compressed data to be written into any unused page space right away, there is no need to try to identify the best combination of compressed data (that tightly fits into a page). By programming each compressed data (without packing with others) after compressing it, the data does not have to occupy the buffer. Also, the removal of packing process can improve the write I/O performance by immediately programming every single compressed data.

%This obviates the need for data packing used in conventional SSDs.


\noindent \emph{\bf Proposed PageSplit+ framework:~}By combining data compression and PageSplit, we now present a practical lifetime-enhanced storage framework, named PageSplit+. The operation of PageSplit+ is illustrated in Figure \ref{fig:compress}. For every write data, it is compressed in a page unit (note that a large I/O write data is split into page-sized chunks, since the flash write unit is a page). The underlying PageSplit flash maintains multiple active blocks, and each active block is supposed to serve data whose size (or whose compression ratio) is in a specific range. As can be seen in Figure \ref{fig:compress}, the active block 1 stores highly-compressed data (i.e., over 75\% compression ratio), whereas the active blocks 2 and 3 serve the data with medium and small compression ratios. As explained in Section \ref{sec:page-alloc}, the unused page spaces in the blocks 2 and 3 can be exploited by data whose compression ratios are higher, since they cannot be consumed by compressed data originally destined to those blocks. For example, three data with compression ratios of 78\%, 88\%, and 65\% are stored in the unused page spaces of the active block 3 (which primarily serves the data with compression ratio between 12.5\% and 50\%). One can see that PageSplit results in very little unused space, and also avoids the high-overhead packing process.


\noindent \emph{\bf Incompressible and small data:~}A few file types exhibit very low compression ratios (see Figure \ref{fig:comp-ratio}). Since compression is not beneficial for such data, PageSplit+ reserves an active block (block 4) to store them without compressing. Also, small write data can get little benefit from compression, since it is the large data that generally achieves higher compression ratios. Hence, PageSplit+ stores small data into the active block with the highest compression ratio range (block 1) without compression.



%One might argue that PageSplit can waste a slight page space, since the size of each compressed data would not be a multiple of sector. However, in the worst case, the page space waste is at most less than a sector for each original data. Note also that the page space waste is commonly observed in the conventional architecture which packs multiple compressed data.


%simplify the flash software as well. For example, the identification of multiple compressed data in a physical page is not necessary in a PageSplit architecture, which makes the design of a compression-based SSDs simple and light-weight. Instead, the address mapping table with the page offset (as described in Section \ref{sec:ftl}) can help one easily find the desired data segment. Therefore, PageSplit is an attractive option that can be used to support data compression in SSDs, as its sector-based programming feature can eliminate the complex and infeasible modules shown in conventional architectures.




