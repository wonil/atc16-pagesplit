




\subsection{Architecture Overview}
The central design concept behind our new architecture is that the sectors that constitute a page are capable of being programmed separately (independent of one another). However, programming a sector independently should not hurt the valid data which is  previously written in other sectors of the page. To achieve this, the shared WL needs be redesigned to partially apply the very high voltage ($V_{pgm}$) to the specified portion. At the same time, $V_{pass}$ (the voltage applied to all other pages) needs to be applied to all of the remaining portions of the WL.

We incorporate a set of \emph{voltage lines} and \emph{switches} to the traditional flash architecture to enable \emph{sector-based programming} without any reliability loss. Figure \ref{ps-archi} depicts a zoomed view which focuses on the first two sectors of three pages in a flash block. The voltage line (VL) is placed between any two sectors and attached to all WLs. A portion of the WL separated by the voltage line matches the sector size, which is referred to as sector line (\emph{ScL}). Either $V_{pgm}$ or $V_{pass}$ can be assigned to each voltage line, depending on which sectors are to be programmed. Furthermore, since the target WL and all the voltage lines are connected, we need a set of switches to regulate the flow of current to prevent conflicts among them. The switches are placed on the WL in both sides of each voltage line. Each switch determines whether the current originated from the neighbor voltage line flows on the ScL or not. All voltage lines and switches are controlled by sector activation logic (SAL), which is implemented in the flash control logic.

The detailed switch design we use in our architecture is also shown in the figure. The switches have a simple design that consists of one NMOS transistor and one PMOS transistor. The latter is connected to ground, while the former is connected to the neighboring VL. The gates of both transistors are supplied with the control input ($V_{in}$) from SAL. The shared node between the two transistors is connected to the ScL. When the control input $V_{in}$ is zero, the ScL is connected to ground; otherwise, the ScL is supplied with the voltage from the VL.



With this support of the additional voltage lines and switches, PageSplit is able to apply the high voltage ($V_{pgm}$) to only the specified ScL(s) and the low voltage ($V_{pass}$) to all the remaining ScL(s) in the target WL. Through this \emph{sector-based programming}, the valid data (which is the data already programmed in specific sectors) will not get hurt by applying $V_{pass}$ to the corresponding ScL, when the other sectors in the same page are being programmed. To program the target sectors (or ScLs) of the target page, a combination of voltage line values and switch on/off is determined by SAL.
%The change in the new architecture is still able to let all other WLs (pages) to be at ($V_{pass}$) as the conventional architecture does.

\begin{figure}
\centering
\includegraphics[scale=.22]{ps-arch}
\caption{Our PageSplit architecture includes the voltage lines and switches, which \emph{separate} two neighboring sectors within a page.}
\label{ps-archi}
\vspace{-10pt}
\end{figure}




\begin{figure}[t]
\centering
\subfloat[Program of Sector 1.]{\label{fig:arch-exam1}\rotatebox{0}{\includegraphics 
[width=0.85\linewidth] {ps-exam1}}}   
%\newline
\hspace{0.1pt}
\subfloat[Program of Sectors 2 and 3.]{\label{fig:arch-exam2}\rotatebox{0}{\includegraphics 
[width=0.98\linewidth]  {ps-exam2}}}
\caption{Two examples of sector-based programs.}
\label{fig:archi-exam}
\vspace{-15pt}
\end{figure}



\subsection{Sector Program Examples}
This section goes over two examples illustrating how the SAL of the flash control logic determines the voltage line values and switch on/off to program the specified sectors. To indicate each voltage line and switch, we need to name them; in an n-sector page flash, the voltage lines are numbered from VL1 to VL\emph{n}, from left to right. Each switch is expressed as (WL\#, VL\#, L/R), since the switches in each WL are placed on the left and right sides of each voltage line.


Figure \ref{fig:arch-exam1} illustrates a sector program (where the target sector is Sector 1 of $WL_{N}$). To apply $V_{pgm}$ to only ScL1 of $WL_{N}$, $V_{pass}$ is given to all the VLs for all other ScLs of $WL_{N}$ and all other WLs. On the other hand, the high voltage for ScL1 of $WL_{N}$ is provided by the original WL source as it would be done in the conventional architecture. By turning off Switch(N, 1, L), the other end of the target ScL is grounded and disconnected from VL1. For all other ScLs (ScL2 to ScL8 of $WL_{N}$ and all ScLs of other WLs), the left-side switches of each VL are tuned off, while the right-side switches are turned on, which makes the current of $V_{pass}$ (provided by each VL) flow.


On the other hand, Figure \ref{fig:arch-exam2} depicts the case of programming both Sectors 2 and 3 of $WL_{N}$. The high voltage $V_{pgm}$ has to be applied to only ScL2 and ScL3 of $WL_{N}$, while all other ScLs of $WL_{N}$ and all other WLs are at $V_{pass}$. To implement this, $V_{pgm}$ is given to VL2, while $V_{pass}$ is applied to VL1 and VL3. To make the current of VL2 flow on ScL2 and ScL3, both Switch(N, 2, L) and Switch(N, 2, R) are turned on, and Switch(N, 1, R) and Switch (N, 3, L) on both ends are grounded. The grounded Switch(N, 1, R) and Switch (N, 3, L) can disconnect the target sectors from VL1 and VL3 (which are at $V_{pass}$) as well. All the other switches around VL2 (i.e., Switch(N-1, 2, L), Switch(N-1, 2, R), Switch(N+1, 2, L), and Switch(N+1, 2, R)) should be turned off to prevent the high voltage current from flowing on other ScLs. Similar to the first example, all other ScLs are given $V_{pass}$ by turning on/off switches. As demonstrated by these examples, \emph{PageSplit allows any sector(s) in a page to be programmed without repeatedly applying the high voltage to the whole WL}.







\subsection{Consumption of Unused Page Space}
\label{sec:archi-io}
In the proposed architecture, any sector can be programmed independently of the programs of other sectors in the same page, since PageSplit can successfully block the intra-page program disturb. This critical feature helps us address the page space waste problem caused by small I/O requests. With this new architecture, a new program can place its data into \emph{any} unused space whose size is equal to or larger than the size of the data. 


%In this way, PageSplit can eliminate flash space wastage by making entire page fully occupied with user I/O data. 


\begin{figure}
\centering
\includegraphics[scale=.2]{ps-io}
\caption{The page consumption scenarios in two architectures. Compared to the conventional one, PageSplit can save two pages by exploiting the unused page space.}
\label{ps-io}
\vspace{-10pt}
\end{figure}

\noindent \emph{\bf Sector selection constraint:~}However, instead of placing write data into any unused page space in the flash, PageSplit limits this ideal case with an important constraint: \emph{in-page-order programming} \cite{page-order,program-distrub-1}. This constraint requires a given set of program commands to be served in the flash pages according to the \emph{pre-defined page sequence within a block}. The purpose of this constraint is to minimize the effect of the \emph{inter-page program disturb}, which indicates the data corruption of the neighboring pages caused by the target page program \cite{program-distrub-1,program-distrub-2,program-distrub-3}. Due to the in-page-order programming constraint, within a block, the unused space in page N is not allowed to be used after the pages whose number is N+1 or higher are programmed. Therefore, the unused space in the current page has to be spent before allocating the next (new) page, since it \emph{cannot} be used later. As a result, some unused space of each page can remain unused, when its size is smaller than the data size of the current program. In spite of the opportunity loss in consuming the entire unused page space, our base architecture sticks to the in-page-order programming to guarantee data integrity (to avoid the inter-page program disturb).
%\footnote{The inter-page program disturb is out of the scope of this work, as the new architecture faithfully follows the in-page-order programming.} 


%To minimize such loss, we propose a new page allocation mechanism described in Section \ref{sec:multi-blocks}.

\noindent \emph{\bf Addressing the small I/O problem:~}PageSplit can significantly reduce the page space waste (brought by small writes), compared to the page-based conventional architecture. Figure \ref{ps-io} provides an example where two architectures serve a series of program commands in a block. The conventional architecture (on the left) quickly consumes the new pages by generating unused space in each page, whereas PageSplit (on the right) reduces the page consumption rate by consuming the remaining space produced by the previous programs in a page. Thus, when serving the six shown program commands, PageSplit can save two new pages by reducing the page space waste, compared to the conventional architecture. Note also that there still exists unused page space in PageSplit, since it complies with the program order. We can conclude that our PageSplit can reduce the space waste caused by small I/O writes, without any reliability loss by considering \emph{both} the intra-page and inter-page program disturb.


\begin{table}
\scriptsize
\begin{tabular}{|p{40pt} ||p{160pt} |}
\hline
Baseline \cite{nand-micron-tlc} & 256 pages (128 WLs), 8KB page (65536 FGTs)  \\
\hline 
PageSplit & 16 VLs in a block, 31 switches in a page  \\
\hline \hline
Voltage lines & 16 VLs / 128 WLs (in a block) = 12.5\%  \\
\hline
Switches & 31 switches / 65536 FGTs (in a page) = 0.04\%  \\
\hline
Switch wires & 128 WLs x 31 switches / 65536 BLs (in a block) = 6.05\%  \\
\hline
\end{tabular}
\vspace{-5pt}
\caption{Hardware overhead brought by PageSplit. To split the 8KB page into sectors, 16 VLs and 31 switches are added to each block and page, respectively.}
\label{tab:hw-overhead}
\vspace{-15pt}
\end{table}


\subsection{Design Feasibility} PageSplit is not designed from scratch, instead it adds low-overhead components to a conventional architecture. 

\noindent \emph{\bf Area:~}PageSplit exploits two types of components: voltage lines and switches. Voltage lines are physically the same as WLs, which are metal lines to flow the current based on the applied voltage. Since their purpose is to divide a page into sectors, the number of voltage lines ($N_{VL}$) is the same as the number of sectors in a page. A set of voltage lines is shared by all the pages in a block. On the other hand, switches are placed on WLs and located in both sides of each voltage line. For the last voltage line, only one switch is put to the left of it, which gives us the total number of switches required for a page as ($N_{VL}\times2-1$). Table \ref{tab:hw-overhead} shows the hardware overhead of PageSplit. Note that the overheads of the added VL, switch, and switch wires are estimated based on the size of the similar underlying flash components, which are WL, FGT, and BL, respectively. The actual space they take is not too much when considering all the resources in a flash memory chip. This overhead can be further reduced, if the programming unit size becomes larger (e.g., 1KB), depending on the design requirements.

\noindent \emph{\bf Power:~}We estimated the energy overhead of PageSplit based on a detailed flash power model \cite{flashpower}, which captures the flash energy consumption behavior based on CACTI \cite{cacti}. In this model, a switch added by PageSplit can be projected as a floating gate, whereas an inserted voltage line can be regarded as an additional bit line. Specifically, since the switches increase the capacitance to be driven along a single WL and the voltage lines increase the number of $BL_{inhibit}$ (discussed in Section \ref{sec:motiv}), the energy consumption of a read or program increases. However, we want to emphasize that both the ratio of switch count to total cell count and the ratio of voltage line count to total bit lines are very small, which results in a negligible energy overhead (i.e., at most 0.1uJ for a flash read or program).



%\subsection{Flash Software Support}
%\label{sec:ftl}
%This new flash architecture brings a slight modification to the flash software. The required changes are twofold: \emph{page allocation} and \emph{address mapping}.


\subsection{Address Mapping}
\label{sec:map-table}
Since any logical I/O data can be placed in any physical page, the flash software implements a mapping mechanism between logical addresses and physical addresses. While PageSplit employs the page as a basic mapping unit, it also allows a sector-based address mapping, since its minimum access unit is a sector. Thus, an entry of PageSplit's mapping table consists of the corresponding physical page number and additionally the \emph{data segment size} and the \emph{start sector position} in the physical page, which is indexed by a logical page or a part of logical page. For example, if a logical data segment whose size is five is programmed in bit positions 3 to 7 in a physical page, the corresponding entry will have a data segment size and a start sector position as 5 and 3, respectively. To specify the two additional items, each entry of the mapping table employed in a 16 sector page-based PageSplit needs 8 additional bits. Note that the entry indexed by a full logical page does not need those additional bits.




%the corresponding entry will have a series of bits, which are encoded as (0, 0, 1, 1, 1, 0, 0, 0). With the data segment in the center of the page, the two sectors on the left and the three sectors on the right might be used by other logical data segments. If a physical page contains an entire logical page data, all the appended bits are set to ``1".






%In the traditional page-based address mapping, an entry of the mapping table consists of the physical page number, page offset, and page size, which are indexed by the logical page number. The physical page number indicates the physical page where the logical page data resides, and the page offset and page size together specify the valid sector portion of the logical page.

%In comparison, PageSplit allows the data segments of multiple different logical pages to reside in the \emph{same} physical page. To distinguish among the different logical page data residing in the same physical page, additional information that captures the position of each logical page data in a page should be included in the address mapping table entry. Since the minimum program unit in PageSplit is a \emph{sector}, a set of bits expressing the \emph{sector positions} in a physical page are appended to the table entry. For example, if a logical page data segment whose size is three is programmed in bit positions 3 to 5 in an 8-sector physical page, the corresponding entry will have a series of bits, which are encoded as (0, 0, 1, 1, 1, 0, 0, 0). 




\subsection{Page Allocation}
\label{sec:page-alloc}
An important functionality of the flash software is to allocate a new flash space to serve a given program data. Based on the specified program order, the conventional flash software allocates a page (called \emph{active page}) of the current block (called \emph{active block}) for a given write-data (even though the data size is smaller than the page size). Once all the pages in an active block are used up, a new active block is allocated, and the first page of the block becomes a new active page for the next program service. 


\noindent \emph{\bf Single active block:~}Unlike the conventional page allocation which provides a new active page for every program, PageSplit allows the following write data to use the unused space in the active page. However, if the remaining page space is not big enough to serve the current program size, a new active page needs to be allocated to serve it. In such a case, the free space in the previous page has to remain \emph{unused}, since a revisit to the space violates the in-page-order programming constraint. This unused page space can be significant, due to diverse sizes of the small I/O writes.


%a new page allocation strategy. As described in Figure \ref{ps-io}, PageSplit's page allocation considers the remaining space of the active page and the current program data size. If the program data size is equal to or smaller than the free space size in the active page, the space is used for the program without allocating a new page. However, if the remaining page space is not big enough to serve the current program size, a new active page is allocated to serve it. In such a case, the free space in the previous page has to remain \emph{unused}, since a revisit to the space after using the neighboring pages violates the in-page-order programming constraint. In this naive page allocation, this unused page space can be significant due to diverse sizes of the small I/O writes. To enhance the unused space reduction capability of PageSplit, we propose a new page allocation strategy. The underlying idea behind this page allocation is to secure ``multiple space candidates" to serve an arbitrary size of program data. Among the multiple space candidates, the fittest space is selected to serve the program data.




\noindent \emph{\bf Multiple active blocks:~}To further reduce the page space waste, PageSplit can maintain multiple active blocks. The underlying idea behind this strategy is to secure ``multiple space candidates" to serve an arbitrary size of program data. Among the multiple space candidates, the fittest space is selected to serve the program data. \emph{Each active block is expected to serve the program data with specific sizes}. There is no need to retain as many active blocks as the number of every possible size. For example, a block to serve data whose size is between half of the page size and page size would be useless, because two such  data cannot be programmed in a physical page, and the remaining portion cannot be used any more. Therefore, \emph{we limit the number of active blocks to the number of data size ``ranges"}. Figure \ref{ps-plus-io} illustrates an 8-sector page flash with three active blocks. Basically, each active block can serve only the designated program sizes; that is, active block 1 serves the program commands with size 1 or 2, whereas active block 2 provides service for programs with a size of 3 or 4. The programs with all other sizes (from 5 to 8) are destined to active block 3. Therefore, multiple blocks can provide more candidate locations that can accommodate the current program size, compared to a single active block.



\begin{figure}
\centering
\includegraphics[scale=.20]{ps-plus-io}
\caption{Page allocation strategies based on single and multiple blocks with an eight-sector page.}
\label{ps-plus-io}
\vspace{-15pt}
\end{figure}


\noindent \emph{\bf Filling out the holes:~}One potential drawback of the program distribution over multiple active blocks is that the active blocks that serve programs with bigger data sizes (particularly, the one that serves the biggest data size range) can generate a lot of unused space. For example, active block 3 of Figure \ref{ps-plus-io} can generate unused space in every page, when it serves programs with data sizes 5, 6, or 7. To fill out the unused space in the active block serving programs with large data, when serving a program with smaller data size, our new page allocation function first checks whether there is an appropriate unused space in the active blocks that are dedicated to serve larger data. For instance, to serve a program with size 4, the active block 3 which serves a data size range of 5 to 8 is inspected first. If an appropriate unused space could not be found there, it is served by active block 2 where the program is originally headed to, based on its size. However, only when the unused space in the active blocks that are dedicated to serve larger data is determined to be ``unavailable" (since it is smaller than the smallest program data size the active block can serve), smaller programs are allowed to be placed into such space. For example, if the unused space size in the active block 3 is 5, a program whose size is 4 cannot use it, since it is still ``available" for the originally dedicated program size 5. Consequently, as shown in Figure \ref{ps-plus-io}, multiple active blocks can significantly reduce the unused page space.%

%This is because even the remaining space generated by the active blocks serving larger ranges is consumed by the smaller programs. For instance, the unavailable space generated by two programs with size 5 in active block 3 are spent by two programs with sizes of 2 and 3, which are originally served by active blocks 1 and 2, respectively.

%Algorithm \ref{alg:ps-plus} describes our new page allocation.


\subsection{Garbage Collection}
\label{sec:garbage-c}
The garbage collection (GC) performed in our PageSplit architecture can expect improved execution time and reduced execution frequency. In PageSplit's GC (consisting of reading valid data portions, merging them, and re-writing them into new flash spaces), the number of reads can be reduced by reading different data portions from different pages of the target block at the same time. Since the proposed architecture can regulate the voltage values of the activated circuits when writing or reading a sector, GC can read two or more valid sectors of different pages at the same time (if their sector offsets do not overlap). Furthermore, PageSplit reduces the page consumption rates by fully utilizing the page space, which in turn decreases the GC invocation frequency.



%Theoretically, different logical page segments (as many as the number of sectors in the page) can share the single physical page space.

%\noindent \emph{\bf Command Format:~} The NAND command used in PageSplit has to additionally specify the sector position in the target page. To construct a program command, the page allocator first returns both the active page and the start position of the remaining space in the page. After updating the address mapping table with the physical page information including the sector positions, the command is delivered to the target flash chip, in the form of (block\#, page \#, sector \#).





%\SetAlFnt{\scriptsize\sf}
%\DecMargin{0.5em}
%\begin{algorithm}[t]
%	\DontPrintSemicolon
%	\SetAlgoVlined
%	\KwIn{Cmd}
%	\KwOut{(activeBlockNum, activePageNum, pageOffset)}
%	\;
%	baseBlkNum := $getBaseBlockNum$(Cmd) \;
%	i := $getNumActiveBlocks()$ \;
%
%	
%	%/* try to find out space in other blocks */ \;
%	\For { i \textgreater baseBlkNum } 
%	{
%		activePageNum	:= $getActivePageNum(i)$\;
%		remainSpace :=  $getRemainSpace(activePageNum)$ \;
%		\If { remainSpace \textless getMinServeSize(i) }
%		{
%			%/* this block has unavailable dummy space */ \;
%			\If { Cmd.size \textless= remainSpace }
%			{
%				%/* the command size fits to the dummy space */ \;
%				activeBlockNum = i \;
%				pageOffset = PageSize - remainSpace \;
%				return \;
%			}
%		}		
%
%		i = i -1 \;
%	}
%	
%	%/*  find out space in the base block */ \;
%	\If {i = baseBlkNum } 
%	{
%		activePageNum	:= $getActivePageNum(baseBlkNum)$\;
%		remainSpace :=  $getRemainSpace(baseBlkNum)$ \;
%		\If { Cmd.size \textgreater remainSpace }
%		{
%			%/* new page is allocated */ \;
%			activePageNum = activePageNum + 1 \;
%			pageOffset = 0 \;
%		}
%		\Else
%		{
%			pageOffset = PageSize - remainSpace \; 
%		}
%	}
%\caption{\footnotesize{The page allocation algorithm employed by PageSplit, which picks up the most %appropriate place from where the current program command can be served.}}
%\label{alg:ps-plus}
%\end{algorithm}

