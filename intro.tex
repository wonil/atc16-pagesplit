
%The higher data transfer rates an interface has, the better it is in the performance perspective. 

The host interface of flash SSDs is evolving, as its internal bandwidth increases based on the increasing internal resources and parallelism. For example, modern high-end SSDs \cite{ref-pciessd-ocz2, ref-pciessd-micron} employ high-bandwidth PCIe \cite{ref-if-pcie4} as their host interface, instead of traditional SATA \cite{ref-if-sata}. More recently, memory channels started to place flash medium closer to processors in the form of DDR DIMM \cite{other-type}. With the debut of Memory Channel Storage (MCS) such as \cite{vimm}, \cite{ultradimm}, and \cite{ArxCis}, it seems that the host interface is no longer a performance bottleneck in flash-based storage systems.\footnote{In this work, MCS is the baseline SSD architecture.}





%As flash-based SSD technologies become mature, they are being employed in a wide range of computing domains. Particularly, large-scale computing environments where the I/O storage system is a major performance bottleneck started to employ flash-based SSDs (e.g., as a cache for the underlying HDD pools), due to their growing performance and storage capacities. Actually, industry has already launched various SSD solutions \cite{cache-micron,cache-hgst,cache-nvelo}, while academia continue to research on the efficient SSD architectures \cite{cache-research2,cache-research3,cache-research4} or mechanisms \cite{cache-research1,cache-research5,cache-research6}, in the large-scale computing environments such as data centers and high-performance clusters. 

 %For example, one popular use case of SSDs in the data center is to exploit it as a cache for the underlying large-scale storage such as HDD pools.


To further increase the storage-internal performance, flash and NAND interfaces also continue to advance. A current design trend in flash interfaces is to improve the bandwidth of flash chip in/out by increasing its frequency. According to the latest ONFi specification 4.0 \cite{onfi4}, the speed of 8-pin flash chip in/out can reach up to 800 MB/s, which is 2x and 6x of ONFi 3.0 \cite{onfi3} and 2.0 \cite{onfi2}, respectively. Meanwhile, another trend \cite{page-trend,page-trend2,page-trend3} is to increase the size of a flash page (even up to 32KB), which is the basic read/write unit for the NAND flash medium. With the increased page size, sequential I/O with large request sizes can achieve higher throughput by reducing the number of reads/writes.


\begin{figure}
\centering
%\vspace{-25pt}
\subfloat[Throughput]{\label{fig:motive-perf}\rotatebox{0}{\includegraphics[scale=0.118]{motive-perf}}}
\hspace{0.01pt}
\subfloat[Lifetime]{\label{fig:motive-lifetime}\rotatebox{0}{\includegraphics[scale=0.118]{motive-lifetime}}}
%\vspace{-5pt}
\caption{Performance and lifetime comparisons under varying page sizes from 2KB to 32KB. Values are normalized to 1KB page configuration.}
\vspace{-12pt}
\label{fig:arch-comp}
\end{figure}





One could observe from Figure \ref{fig:motive-perf} the performance benefits of adopting large page sizes. This figure shows that the SSD performance improves until a certain point when increasing the page size. Particularly, sequential writes (\emph{seqw}) get considerable benefits from large pages, since writes to NAND array are costlier than reads.



%To further increase the performance benefits of SSDs employed in such environments, one major SSD design trend is to adopt large page sizes (even up to 32KB) \cite{page-trend,page-trend2,page-trend3}. Since a page is the basic unit of flash read and write, the larger the page size is, the higher throughput an SSD can achieve. In addition, to fully utilize the increased page size, file systems are generally reconfigured to generate sequential I/O with large request sizes \cite{filesys-clustering1,filesys-clustering2,filesys-clustering3}. Specifically, by increasing I/O block sizes or clustering small I/O requests, large I/O requests whose sizes are at least equal to or bigger than the page size can be generated at the host level. It is noted that (i) the increasing page size and (ii) the corresponding efforts to increase I/O size are current directions to achieve high I/O throughput when using SSDs in big data environments.


%However, recent analysis \cite{dc-io1,dc-io2,dc-io3} on enterprise-scale file systems revealed that there are cases which goes against the continuing efforts towards generating and processing sequential I/O with large sizes. In large-scale computing environments, a large number of users can create and manipulate files, which results in massive updates of file metadata. The I/O requests caused by the metadata updates are usually random small writes whose request sizes are smaller than the page size (i.e., at most 4KB).\footnote{In this paper, ``small I/O" means a data size which is smaller than the SSD page size.} In fact, according to \cite{dc-io1}, such small writes related to metadata updates take more than 50\% of all file I/O. In addition to the originally-small writes, even sequential I/O writes with large request sizes can generate a chunk of small data (i.e., a tail portion of the whole data), if the I/O size is not a multiple of the page size.


Unfortunately, increasing the page size is a serious threat to the limited SSD lifetime. Figure \ref{fig:motive-lifetime} shows that increasing the page size reduces the lifetime of different applications. This is because a larger page generates more partial updates (or small writes), which in turn brings additional ``read-modify-write" operations. More specifically, since basic flash read/write unit is a page, partial updates whose request sizes are smaller than the page size are performed by (1) reading the whole page data, (2) modifying the portion to be updated, and (3) writing the whole page data including the unmodified portion. Rewriting these unmodified data unnecessarily spends the finite flash write counts, which exacerbates with increasing page sizes. Furthermore, the partial updates have also a negative impact on performance, because they waste storage bandwidth by generating additional reads. This is the reason why throughput values in Figure \ref{fig:motive-perf} decrease beyond a certain point.

% applications with diverse I/O access patterns\footnote{Sequential accesses are dominant in eigen and proj.} accelerate the end of SSD life, as page size increases from 2KB to 32KB.


One might suggest that such waste of page space could be eliminated by serving a partial update from a new page (and just invalidating the old data portion) and by serving other I/O data from the unused portion of the page. In contrast to read-modify-write operations, placing different partial updates into a single page could prevent storage lifetime and bandwidth from being wasted. However, this strategy is not feasible in practice, since current flash architectures prohibit multiple accesses (by different partial updates) to a page before the whole page is erased (even though they use different portions in the page). This is because the repeated accesses to a page can corrupt the previously-written data in the page.



% such small I/O writes waste flash page space, which is a serious threat to the limited SSD lifetime. When writing small data in a new page, the unused portion of the page remains empty (or is filled with dummy data).\footnote{Note that ``Read-Modify-Write" \cite{ssd-ext} also wastes page space by rewriting the unmodified data portion. Even worse, it wastes the storage bandwidth by generating additional read as well.} However, this unused portion of the page cannot be exploited by any other I/O data, since the current flash architectures prohibit multiple accesses to a page, before the whole page is erased. This is because repeated accesses to a page can corrupt the previously-written data in the page, even though they are not over-written.\footnote{The data disturbance caused by accesses to other space in the page motivates us to devise a new flash architecture which isolates a page portion from the others in the page, which is described in Section \ref{sec:motiv}.} Consequently, the small writes amplify the data volume written to an SSD by generating the unused portions of pages. To make matters worse, this page space waste becomes even more destructive as the page size increases. As can be seen in Figure \ref{fig:misalign-page-sense}, the volume of the used flash pages is significantly amplified, (compared to the original I/O data volume), when larger pages are employed. Therefore, \emph{the conventional flash architecture experiencing an increasing number of small writes in large-scale computing domains reduces SSD lifetime by wasting the page space}.



%Such small I/O writes submitted to the SSD with a large page can be processed in two ways. A naive method first reads the target page and merges the valid portion of the old data with the new data, which is followed by writing the merged data into a new page. This ``Read-Merge-Write" wastes not only the storage bandwidth by generating additional read but also the page space (and SSD lifetime\footnote{One way to prolong the use of the lifetime-limited SSD is to use the flash space as efficiently as possible.}) by \emph{rewriting the unmodified data portion}. Alternatively, ``partial write" can save the SSD bandwidth by writing the new data in a new page and invalidating only the old data portion, if a fine-grained addressing that can specify the page index is supported. 



%Unfortunately, ``partial write" also wastes flash page space, which still remains a threat to the limited SSD lifetime. When writing small data in a new page, the unused portion of the page remains empty (or is filled with dummy data). However, the unused part of the page cannot be exploited by any other I/O data, since the current flash architecture prohibits multiple accesses to a page, before the whole page is erased. This is because repeated accesses to a page can corrupt the previously-written data in the page, even though they are not over-written. Consequently, the small writes amplify the data volume written to SSD by generating the unused portions of pages. To make matters worse, this page space waste becomes even more destructive as the page size increases. As can be seen in Figure \ref{fig:misalign-page-sense}, the volume of the used flash pages is significantly amplified, (compared to the original I/O data volume), when larger pages are employed. Therefore, \emph{the conventional flash architecture experiencing an increasing number of small writes in data centers reduces SSD lifetime by wasting the page space}.



%\begin{figure}
%\centering
%\includegraphics[width=0.75\linewidth]{misalign-page-sense}
%\caption{The amplified data volume caused by small I/O requests under varying page sizes from 2KB to 32KB.}
%\label{fig:misalign-page-sense}
%\end{figure}



%One might suggest that such waste of page space could be resolved by data merging (or combining) at the buffer inside the SSD \cite{ram-caching}. However, this becomes an imperfect solution in the large-scale computing environments where the pressure on the RAM buffer is too high to buffer I/O data for long periods of time. Specifically, the high degree of internal parallelism and the increasing page sizes are likely to generate severe contention to load a large volume of data in the buffer. Note also that read-data and flash management software such as FTL are also the major users of the RAM inside the SSD. The expected high contention among these users of the RAM prevents small I/O data from being staged and merged, which leads to prompt flash writes without merging.


Therefore, to enjoy the performance benefit coming from increasing page sizes without sacrificing the SSD lifetime, one needs more innovative solutions that allow each page space to be fully consumed by multiple partial I/O requests without incurring any data corruption. To realize this, we propose a new flash architecture, called {\bf PageSplit}, which requires a slight modification to the conventional flash architecture. Specifically, PageSplit employs a large page to achieve high throughput from sequential I/O, while it allows each sector in a page to be accessed separately to prevent the waste of page space by small (partial) I/O (i.e., their request sizes are smaller than the page size). By adapting to both large (sequential) and small (partial update) I/O, a storage system based on PageSplit can achieve both high performance and prolonged lifespan. It is also to be noted that the proposed flash architecture can be employed in any type of storage, ranging from the traditional SSDs such as SATA- and PCIe-SSDs to state-of-the-art storage formats such as MCS and all flash arrays, since our proposal enhances the ``flash internals".


%splits a given page into \emph{sectors} and allows each sector in the page to be programmed separately. This sector-based programming (instead of the traditional page-based one) can allow other small I/O data to use the unused page space without hurting the data integrity of the neighboring sectors. Consequently, this new flash architecture can minimize the page space waste caused by small I/O data, which can in turn prolong the SSD life. Note also that PageSplit can still enjoy the throughput benefit coming from the increased page size, since it maintains the native (original) page unit for large I/O data.


%Even though PageSplit helps to reduce the wasted space in pages, the diversity in I/O data size and the program sequence constraint can potentially limit the benefits of this new architecture. Since data sizes can vary significantly, it is necessary to find the best target place, whose size is equal to or bigger than the required size. However, the program sequence constraint excessively limits the potential candidate places. To fully expose the potential of PageSplit, we also propose a software solution on top of our PageSplit architecture, named {\bf PageSplit+}, which monitors multiple candidate places with different sizes and allocates the most appropriate place for the current I/O data. With PageSplit+, the wasted space in the SSD can be further reduced. 


%One point we want to emphasize is that our PageSplit architecture can be employed in other types of flash-based systems as well. For example, Solid State Module (SSM) \cite{other-type} such as \cite{vimm}, \cite{ultradimm}, and \cite{ArxCis}, is an effort to place flash medium closer to processors via memory bus (i.e., DDR DIMM). Since SSMs would handle memory unit-based data whose sizes are smaller than conventional block I/O data sizes, significant page space waste can be generated. Note also that these architectures generally employ a small or no internal buffer, which could be used to reduce the waste of page space. Accordingly, our PageSplit architecture can be very successful in prolonging SSD lifetimes in such architectures. Our main {\bf contributions} can be summarized as follows:


We also present a lifetime-enhanced storage framework, called {\bf PageSplit+}, by combining data compression and PageSplit architecture. Even though data compression has been widely studied in SSDs \cite{compress-1, compress-2, compress-3} as a way to prolong the SSD lifetime, these prior works are based on a set of assumptions and ideal cases. For example, it is necessary to generate a chunk of data to fit to a page by packing multiple compressed data, since data compression reduces the original data size. This is not trivial in practice, since a wide range of compression ratios in I/O workloads make building a pack of compressed data whose size is similar to the page size very difficult. In contrast, PageSplit's access unit adapts to any size of compressed I/O data, and thus we can store it in NAND medium without packing it with other I/O data. Our contributions can be summarized as follows:
 %Our proposed flash architecture makes data compression-based SSD more feasible.



\begin{figure*}[t]
\centering
\includegraphics[scale=.25]{ssd-internal}
\caption{MCS internals: from the host interface to the NAND cell.}
\label{ssd-internal}
\vspace{-10pt}
\end{figure*}


%\noindent $\bullet$ \emph{An architectural solution for partial writes:~} To mitigate the waste of page space caused by rampant small writes in the large-scale computing domains, we propose a new flash architecture, PageSplit, which enables multiple accesses to the same page via \emph{sector-based programming}. The proposed architecture enabling sectors to be programmed separately without corrupting the neighboring sectors allows the unused space to be used by other I/O requests. By minimizing the flash space wasted by small I/O requests, PageSplit can improve the SSD lifetime by 38\% on average.


\noindent$\bullet$ \emph{A new flash architecture to improve both performance and lifetime:~}To extract high throughput from large page sizes, but also to conserve the limited lifetime, we propose a new low-overhead flash architecture, PageSplit, which provides large pages for sequential I/O and enables multiple accesses to the same page for small I/O via a finer-granularity (sector-based) programming. With a slight architecture-level modification, PageSplit can improve throughput and lifetime by on average 54\% and 43\%, respectively, across a variety of SSD workloads. To better utilize PageSplit, we also provide new garbage collection and page allocation mechanisms.

 
%\noindent $\bullet$ \emph{An enhanced software for PageSplit:~}  To extract the full potential of PageSplit, an additional software mechanism, PageSplit+, is presented on top of the proposed architecture. To cope with diverse sizes of the small writes, PageSplit+ keeps multiple place candidates with different sizes, and allocates the best place where the data size fits. With the help of PageSplit+, the data volume amplification by the page space waste can be reduced by 43\% on average.


\noindent$\bullet$ \emph{A new storage framework to prolong lifetime:~}By leveraging the adaptive programming units of PageSplit in league with a data compression strategy, we propose a lifetime-enhanced storage framework, called PageSplit+. Since PageSplit allows each compressed data to be stored without packing it with others (regardless of its data compression ratio), PageSplit+ can make data compression in SSDs practical.



%\noindent $\bullet$ \emph{Discussing an alternate use of the new flash architecture:~} The proposed architecture can have another useful application, which is the data compression-based SSD. Such SSDs accumulate the compressed data (which can be smaller than the page size) to fully utilize the page space, which brings significant overheads in terms of the performance and design complexity. PageSplit with its sector-based programming unit can be used to program each compressed data without packing it with the other ones, which makes the compression-based SSD design simple and feasible.


